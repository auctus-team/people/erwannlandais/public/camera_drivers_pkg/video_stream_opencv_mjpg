/*
 * Software License Agreement (Modified BSD License)
 *
 *  Copyright (c) 2016, PAL Robotics, S.L.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of PAL Robotics, S.L. nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Sammy Pfeiffer
 */

#include <ros/ros.h>
#include <nodelet/nodelet.h>
#include <dynamic_reconfigure/server.h>
#include <image_transport/image_transport.h>
#include <camera_info_manager/camera_info_manager.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <cv_bridge/cv_bridge.h>
#include <sstream>
#include <stdexcept>
#include <boost/filesystem.hpp>
#include <boost/assign/list_of.hpp>
#include <boost/thread/thread.hpp>
#include <queue>
#include <mutex>
#include <video_stream_opencv_mjpg/VideoStreamConfig.h>

#include <iostream>
#include <stdexcept>
#include <stdio.h>
#include <ctime>


#include <numeric>

namespace fs = boost::filesystem;

namespace video_stream_opencv_mjpg {

class VideoStreamNodelet: public nodelet::Nodelet {
protected:
boost::shared_ptr<ros::NodeHandle> nh, pnh;
image_transport::CameraPublisher pub;
boost::shared_ptr<dynamic_reconfigure::Server<video_stream_opencv_mjpg::VideoStreamConfig> > dyn_srv;
video_stream_opencv_mjpg::VideoStreamConfig config;
std::mutex q_mutex, s_mutex, c_mutex, p_mutex;
std::mutex sv_mutex;
std::queue<cv::Mat> framesQueue;
std::queue<cv::Mat> videoFramesQueue;
cv::Mat frame;
boost::shared_ptr<cv::VideoCapture> cap;


std::string video_stream_provider;
std::string video_stream_provider_type;
int subscriber_num;
bool capture_thread_running;
boost::thread capture_thread;
bool saving_thread_running;
boost::thread saving_thread;

ros::Timer publish_timer;
sensor_msgs::CameraInfo cam_info_msg;
bool Gstreamer_ok;

std::string scriptPath;
bool activateRecord;
bool activateAudioRecord;
std::string ndate;
std::string previewName;
std::string path2Save;
bool startupNotThread;

bool pubStats = false;

// I decided that width / height / fps should stay blocked

int width;
int height;
int fps;
std::string codec;
std::vector<std::string> admissibleCodecs = {"MJPG","H264","RAW"};
std::string set_backend;

int origin_fps;
std::vector<std::string> v4l2_params;

// Based on the ros tutorial on transforming opencv images to Image messages

std::vector<std::string> splitString(
    std::string str,
    std::string delimeter)
{
    std::vector<std::string> splittedStrings = {};
    size_t pos = 0;

    while ((pos = str.find(delimeter)) != std::string::npos)
    {
        std::string token = str.substr(0, pos);
        if (token.length() > 0)
            splittedStrings.push_back(token);
        str.erase(0, pos + delimeter.length());
    }

    if (str.length() > 0)
        splittedStrings.push_back(str);
    return splittedStrings;
}


virtual sensor_msgs::CameraInfo get_default_camera_info_from_image(sensor_msgs::ImagePtr img){
    sensor_msgs::CameraInfo cam_info_msg;
    cam_info_msg.header.frame_id = img->header.frame_id;
    // Fill image size
    cam_info_msg.height = img->height;
    cam_info_msg.width = img->width;
    NODELET_INFO_STREAM("The image width is: " << img->width);
    NODELET_INFO_STREAM("The image height is: " << img->height);
    // Add the most common distortion model as sensor_msgs/CameraInfo says
    cam_info_msg.distortion_model = "plumb_bob";
    // Don't let distorsion matrix be empty
    cam_info_msg.D.resize(5, 0.0);
    // Give a reasonable default intrinsic camera matrix
    cam_info_msg.K = boost::assign::list_of(img->width/2.0) (0.0) (img->width/2.0)
            (0.0) (img->height/2.0) (img->height/2.0)
            (0.0) (0.0) (1.0);
    // Give a reasonable default rectification matrix
    cam_info_msg.R = boost::assign::list_of (1.0) (0.0) (0.0)
            (0.0) (1.0) (0.0)
            (0.0) (0.0) (1.0);
    // Give a reasonable default projection matrix
    cam_info_msg.P = boost::assign::list_of (img->width/2.0) (0.0) (img->width/2.0) (0.0)
            (0.0) (img->height/2.0) (img->height/2.0) (0.0)
            (0.0) (0.0) (1.0) (0.0);
    return cam_info_msg;
}

virtual void do_saving() {
    NODELET_DEBUG("Saving video thread started");

    bool startup = true;
    cv::VideoWriter out;
    video_stream_opencv_mjpg::VideoStreamConfig latest_config;
    saving_thread_running = true;
    while (nh->ok() && saving_thread_running) {

      if (subscriber_num>0)
      {

        if (startup)
        {
          cv::Size size(width,height);

            time_t rawtime;
            struct tm * timeinfo;
            char buffer[80];

            time (&rawtime);
            timeinfo = localtime(&rawtime);

            strftime(buffer,80,"%Y%m%d_%H%M",timeinfo);
            std::string str(buffer);
          
          ndate = str;

          std::cout << ndate << std::endl;

          std::string saveName = path2Save + "/"+ ndate +"_output_"+ previewName+".avi";
          //std::cout << "herebef" << std::endl;
          std::string gst_out = "appsrc ! video/x-raw, format=BGR ! queue ! videoconvert ! x264enc ! h264parse ! mpegtsmux ! filesink location="+saveName;
          // ouvre mais ecrit pas
          //cv::VideoWriter out(gst_out,cv::CAP_GSTREAMER, cv::VideoWriter::fourcc('M', 'J', 'P', 'G'), fps, size );    
          // ouvre, mais bug KO dès ecriture
          out.open(gst_out,cv::CAP_GSTREAMER, 0, fps, size );  
          out.write(frame);
          // ouvre pas
          //cv::VideoWriter out(saveName, cv::CAP_V4L2, cv::VideoWriter::fourcc('M', 'J', 'P', 'G'), fps, size );        
          // OK, mais louuuuuurd
          //out.open(saveName, cv::VideoWriter::fourcc('M', 'J', 'P', 'G'), fps, size );        
          // OK, mais louuuuuurd
          //out.open(saveName, cv::CAP_OPENCV_MJPEG, cv::VideoWriter::fourcc('M', 'J', 'P', 'G'), fps, size ); 


          //out.open(saveName,0, fps, size );    
          if (! out.isOpened())
          {
            std::cout << "not open" << std::endl;
          }     
          else
          {
            std::cout << "open, ok" << std::endl;
          }
          startup = false;     
        }


        bool is_new_image = false;
        {
          std::lock_guard<std::mutex> lock(sv_mutex);
          latest_config = config;
        }

        {
            std::lock_guard<std::mutex> g(q_mutex);
            if (!videoFramesQueue.empty()){
                frame = videoFramesQueue.front();
                videoFramesQueue.pop();
                is_new_image = true;
            }
        }

        // Check if grabbed frame is actually filled with some content
        if(!frame.empty() && is_new_image) {
            // From http://docs.opencv.org/modules/core/doc/operations_on_arrays.html#void flip(InputArray src, OutputArray dst, int flipCode)
            // FLIP_HORIZONTAL == 1, FLIP_VERTICAL == 0 or FLIP_BOTH == -1
            // Flip the image if necessary
            if (latest_config.flip_horizontal && latest_config.flip_vertical)
              cv::flip(frame, frame, -1);
            else if (latest_config.flip_horizontal)
              cv::flip(frame, frame, 1);
            else if (latest_config.flip_vertical)
              cv::flip(frame, frame, 0);

            out.write(frame);

            if (activateAudioRecord)
            {
              std::string command = scriptPath+"/run_audio.sh &";
              system(command.c_str());
              activateAudioRecord = false;
            }
        }
      }
    }
    out.release();
}

virtual void do_capture() {
    NODELET_DEBUG("Capture thread started");
    cv::Mat frame;
    video_stream_opencv_mjpg::VideoStreamConfig latest_config = config;
    ros::Rate camera_fps_rate(latest_config.set_camera_fps);
    std::vector<float> durTime;
    int frame_counter = 0;
    // Read frames as fast as possible
    capture_thread_running = true;
    while (nh->ok() && capture_thread_running && subscriber_num > 0) {
        {
          std::lock_guard<std::mutex> lock(c_mutex);
          latest_config = config;
        }
        if (!cap->isOpened()) {
          NODELET_WARN("Waiting for device...");
          cv::waitKey(100); 
          continue;
        }
        double begin = ros::Time::now().toSec();
        if (!cap->read(frame)) {
          NODELET_ERROR_STREAM_THROTTLE(1.0, "Could not capture frame (frame_counter: " << frame_counter << ")");
          if (latest_config.reopen_on_read_failure) {
            NODELET_WARN_STREAM_THROTTLE(1.0, "trying to reopen the device");
            unsubscribe();
            subscribe();
          }
        }
        float timeCap =  ros::Time::now().toSec() - begin;
        durTime.push_back( timeCap);
        if (durTime.size() >= fps)
        {
          float value = std::accumulate(durTime.begin(), durTime.end(), 0.0)/durTime.size();
          if (pubStats)
          {
            std::cout << "Duration for capture on "<< fps << " values : " << value << std::endl;
          }
          durTime.clear();
        }

        frame_counter++;
        if (video_stream_provider_type == "videofile")
        {
            camera_fps_rate.sleep();
        }
        NODELET_DEBUG_STREAM("Current frame_counter: " << frame_counter << " latest_config.stop_frame - latest_config.start_frame: " << latest_config.stop_frame - latest_config.start_frame);
        if (video_stream_provider_type == "videofile" &&
            frame_counter >= latest_config.stop_frame - latest_config.start_frame)
        {
            if (latest_config.loop_videofile)
            {
                cap->open(video_stream_provider);
                cap->set(cv::CAP_PROP_POS_FRAMES, latest_config.start_frame);
                frame_counter = 0;
                NODELET_DEBUG("Reached end of frames, looping.");
            }
            else {
              NODELET_INFO("Reached the end of frames");
              break;
            }
        }

        if(!frame.empty()) {
            std::lock_guard<std::mutex> g(q_mutex);
            // accumulate only until max_queue_size
            while (framesQueue.size() >= latest_config.buffer_queue_size) {
              framesQueue.pop();
            }
            framesQueue.push(frame.clone());
            while (videoFramesQueue.size() >= latest_config.buffer_queue_size) {
              videoFramesQueue.pop();
            }
            videoFramesQueue.push(frame.clone());            
        }
    }
    NODELET_DEBUG("Capture thread finished");
}

virtual void do_publish(const ros::TimerEvent& event) {
    bool is_new_image = false;
    sensor_msgs::ImagePtr msg;
    std_msgs::Header header;
    video_stream_opencv_mjpg::VideoStreamConfig latest_config;

    {
      std::lock_guard<std::mutex> lock(p_mutex);
      latest_config = config;
    }

    header.frame_id = latest_config.frame_id;
    {
        std::lock_guard<std::mutex> g(q_mutex);
        if (!framesQueue.empty()){
            frame = framesQueue.front();
            framesQueue.pop();
            is_new_image = true;
        }
    }

    // Check if grabbed frame is actually filled with some content
    if(!frame.empty() && is_new_image) {
        double begin = ros::Time::now().toSec();
        // From http://docs.opencv.org/modules/core/doc/operations_on_arrays.html#void flip(InputArray src, OutputArray dst, int flipCode)
        // FLIP_HORIZONTAL == 1, FLIP_VERTICAL == 0 or FLIP_BOTH == -1
        // Flip the image if necessary
        if (latest_config.flip_horizontal && latest_config.flip_vertical)
          cv::flip(frame, frame, -1);
        else if (latest_config.flip_horizontal)
          cv::flip(frame, frame, 1);
        else if (latest_config.flip_vertical)
          cv::flip(frame, frame, 0);
        cv_bridge::CvImagePtr cv_image =
          boost::make_shared<cv_bridge::CvImage>(header, "bgr8", frame);
        if (latest_config.output_encoding != "bgr8")
        {
          try {
            // https://github.com/ros-perception/vision_opencv/blob/melodic/cv_bridge/include/cv_bridge/cv_bridge.h#L247
            cv_image = cv_bridge::cvtColor(cv_image, latest_config.output_encoding);
          } catch (std::runtime_error &ex) {
            NODELET_ERROR_STREAM("cannot change encoding to " << latest_config.output_encoding
                                 << ": " << ex.what());
          }
        }
        msg = cv_image->toImageMsg();
        // Create a default camera info if we didn't get a stored one on initialization
        if (cam_info_msg.distortion_model == ""){
            NODELET_WARN_STREAM("No calibration file given, publishing a reasonable default camera info.");
            cam_info_msg = get_default_camera_info_from_image(msg);
        }
        // The timestamps are in sync thanks to this publisher
        pub.publish(*msg, cam_info_msg, ros::Time::now());
    }
}

virtual void subscribe() {
  ROS_DEBUG("Subscribe");
  video_stream_opencv_mjpg::VideoStreamConfig& latest_config = config;
  // initialize camera info publisher
  camera_info_manager::CameraInfoManager cam_info_manager(
      *nh, latest_config.camera_name, latest_config.camera_info_url);
  // Get the saved camera info if any
  cam_info_msg = cam_info_manager.getCameraInfo();
  cam_info_msg.header.frame_id = latest_config.frame_id;

  // initialize camera
  cap.reset(new cv::VideoCapture);

  std::string backend =set_backend;

  try {

    int device_num = std::stoi(video_stream_provider);

    NODELET_INFO_STREAM("Opening VideoCapture with dev provider: /dev/video" << device_num);


    if (backend =="gstreamer")
    {
          NODELET_INFO_STREAM("First try with Gstreamer");
      //std::string str_command = "v4l2src device=/dev/video" +std::to_string(device_num)+" ! image/jpeg, width="+std::to_string(width)+", height="+std::to_string(height)+", framerate="+std::to_string(fps)+"/1, format=MJPG ! avdec_mjpeg ! videoconvert ! video/x-raw, format=BGR ! appsink";    

      std::string str_command = "";
      
      if (codec == "MJPG")
      {
       str_command = "v4l2src device=/dev/video" +std::to_string(device_num)+" io-mode=2 ! image/jpeg, width="+std::to_string(width)+", height="+std::to_string(height)+", framerate="+std::to_string(fps)+"/1, format=MJPG ! jpegdec ! videoconvert ! video/x-raw, format=BGR ! appsink";         
      }
      else if (codec == "H264")
      {
        // from https://stackoverflow.com/questions/15787967/capturing-h-264-stream-from-camera-with-gstreamer
          str_command = "v4l2src device=/dev/video" +std::to_string(device_num)+" ! video/x-h264, width="+std::to_string(width)+", height="+std::to_string(height)+", framerate="+std::to_string(fps)+"/1 ! queue ! h264parse! avdec_h264 ! queue ! videoconvert ! video/x-raw, format=BGR ! appsink";         
           //     str_command = "v4l2src device=/dev/video" +std::to_string(device_num)+" ! video/x-h264, width="+std::to_string(width)+", height="+std::to_string(height)+", framerate="+std::to_string(fps)+"/1 ! queue !  avdec_h264 ! queue ! videoconvert ! video/x-raw, format=BGR ! appsink";         
      

      }

            std::cout << str_command << std::endl;
      
      cap->open(str_command,cv::CAP_GSTREAMER);

      if (!cap->isOpened()) {
        NODELET_INFO_STREAM("Error with Gstreamer. Let's try with V4L2.");
        cap->open(device_num, cv::CAP_V4L2);
        Gstreamer_ok = false;
      }
      else{
           NODELET_INFO_STREAM("Gstreamer OK");
        Gstreamer_ok = true;
      }
    }
    else
    {
                NODELET_INFO_STREAM("First try with V4L2");
      cap->open(device_num, cv::CAP_V4L2);
      Gstreamer_ok = false;
    }


    //cap->open(device_num);    


  } catch (std::invalid_argument &ex) {
    NODELET_INFO_STREAM("Opening VideoCapture with video provider: " << video_stream_provider);


    if(video_stream_provider_type == "videofile" )
      {
        // We can only check the number of frames when we actually open the video file
        NODELET_INFO_STREAM("Video number of frames: " << cap->get(cv::CAP_PROP_FRAME_COUNT));
        if(latest_config.stop_frame == -1)
        {
          NODELET_WARN_STREAM("'stop_frame' set to -1, setting internally (won't be shown in dynamic_reconfigure) to last frame: " << cap->get(cv::CAP_PROP_FRAME_COUNT));
          latest_config.stop_frame = cap->get(cv::CAP_PROP_FRAME_COUNT);
        }
        if(latest_config.stop_frame > cap->get(cv::CAP_PROP_FRAME_COUNT))
          {
            NODELET_ERROR_STREAM("Invalid 'stop_frame' " << latest_config.stop_frame << " for video which has " << cap->get(cv::CAP_PROP_FRAME_COUNT) << " frames. Setting internally (won't be shown in dynamic_reconfigure) 'stop_frame' to " << cap->get(cv::CAP_PROP_FRAME_COUNT));
            latest_config.stop_frame = cap->get(cv::CAP_PROP_FRAME_COUNT);
          }

        if(latest_config.start_frame >= latest_config.stop_frame)
          {
            NODELET_ERROR_STREAM("Invalid 'start_frame' " << latest_config.start_frame << ", which exceeds 'stop_frame' " << latest_config.stop_frame << ". Setting internally (won't be shown in dynamic_reconfigure) 'start_frame' to 0.");
            latest_config.start_frame = 0;
          }

        cap->set(cv::CAP_PROP_POS_FRAMES, latest_config.start_frame);
      }
    if (!cap->isOpened()) {
      NODELET_FATAL_STREAM("Invalid 'video_stream_provider': " << video_stream_provider);
      return;
    }
  }
  NODELET_INFO_STREAM("Video stream provider type detected: " << video_stream_provider_type);

  if (!Gstreamer_ok)
  {
    if (codec != "RAW")
    {
      cap->set(cv::CAP_PROP_FOURCC, cv::VideoWriter::fourcc(codec[0],codec[1],codec[2],codec[3]));
      //cap->set(cv::CAP_PROP_FOURCC, cv::VideoWriter::fourcc('M', 'J', 'P', 'G'));
      int fourcc = cap->get(cv::CAP_PROP_FOURCC);
      char c1 = fourcc & 255;
      char c2 = (fourcc >> 8) & 255;
      char c3 = (fourcc >> 16) & 255;
      char c4 = (fourcc >> 24) & 255;

      std::vector<char> conc = {c1,c2,c3,c4};

      int i = 0;
      bool ok = true;

      while (i < conc.size() && ok)
      {
        if (conc[i] != codec[i])
        {
          ok = false;
        }
        i+=1;
      }

      if (ok)
      {
         NODELET_INFO_STREAM("Type set : " << codec);
      }
      else
      {
        NODELET_INFO_STREAM("Error : type asked : " << codec << " ; current type of camera : " << c1 << c2 << c3 << c4 );
      }

    }
  }

  double reported_camera_fps;
  // OpenCV 2.4 returns -1 (instead of a 0 as the spec says) and prompts an error
  // HIGHGUI ERROR: V4L2: Unable to get property <unknown property string>(5) - Invalid argument
  reported_camera_fps = cap->get(cv::CAP_PROP_FPS);
  if (reported_camera_fps > 0.0)
    NODELET_INFO_STREAM("Camera reports FPS: " << reported_camera_fps);
  else
    NODELET_INFO_STREAM("Backend can't provide camera FPS information");
  
  if (!Gstreamer_ok)
  {

    NODELET_INFO_STREAM("Ask for FPS: " <<  latest_config.set_camera_fps) ;
    cap->set(cv::CAP_PROP_FPS, latest_config.set_camera_fps);
        NODELET_INFO_STREAM("New camera reports FPS: " << cap->get(cv::CAP_PROP_FPS) ) ;
    if(!cap->isOpened()){
      NODELET_ERROR_STREAM("Could not open the stream.");
      return;
    }
    if (width != 0 && height != 0){
      cap->set(cv::CAP_PROP_FRAME_WIDTH, width);
      cap->set(cv::CAP_PROP_FRAME_HEIGHT, height);
            NODELET_INFO_STREAM("Width set : " <<cap->get(cv::CAP_PROP_FRAME_WIDTH));
    }
  }


  std::vector<video_stream_opencv_mjpg::VideoStreamConfig::AbstractParamDescriptionConstPtr> allParams = latest_config.__getParamDescriptions__();

  // boost::any val;
  // std::string paramName = boost::any_cast<std::string>(allParams[0]->name);
  // std::string paramType = boost::any_cast<std::string>(allParams[0]->type);
  // allParams[0]->getValue(latest_config,val);
  // std::string paramVal;
  // if (paramType == "int")
  // {
  //   paramVal =std::to_string(  boost::any_cast<int>(val) );
  // }
  // else if (paramType == "double")
  // {
  //   paramVal =std::to_string(  boost::any_cast<double>(val) );
  // }  
  // else if (paramType == "bool")
  // {
  //   paramVal =std::to_string(  boost::any_cast<bool>(val) );
  // }    

 
  for (int i = 0; i < allParams.size(); i++)
  {
    if ( std::find(v4l2_params.begin(), v4l2_params.end(), allParams[i]->name) != v4l2_params.end())
    {

      boost::any val;
      std::string paramName = boost::any_cast<std::string>(allParams[i]->name);
      std::string paramType = boost::any_cast<std::string>(allParams[i]->type);
      allParams[i]->getValue(latest_config,val);
      std::string paramVal;
      if (paramType == "int")
      {
        paramVal =std::to_string(  boost::any_cast<int>(val) );
      }
      else if (paramType == "double")
      {
        paramVal =std::to_string(  boost::any_cast<double>(val) );
      }  
      else if (paramType == "bool")
      {
        paramVal =std::to_string(  boost::any_cast<bool>(val) );
      }    
      std::string command = "v4l2-ctl -d /dev/video" + video_stream_provider + " -c " + paramName+"=" + paramVal;
      char buffer[128];
      std::string result = "";    
      FILE* pipe = popen(command.c_str(), "r");

      // read till end of process:
      while (!feof(pipe)) {

          // use buffer to read and add to result
          if (fgets(buffer, 128, pipe) != NULL)
            result += buffer;
      }

      if (result != "")
      {
         NODELET_WARN_STREAM("(" << paramName << "," << paramVal << ") : " << result);
      }



      // read till end of process:
      // while (!feof(pipe)) {
      //   continue;
      // }
      pclose(pipe);
      // if (i==0)
      // {
      // std::cout << paramName << " : " << paramVal << " : " << result << std::endl;
      // }
      
    }


  }




  try {
    capture_thread = boost::thread(
      boost::bind(&VideoStreamNodelet::do_capture, this));
 
    publish_timer = nh->createTimer(
      ros::Duration(0.00001), &VideoStreamNodelet::do_publish, this);      

    // publish_timer = nh->createTimer(
    //   ros::Duration(1.0 / latest_config.fps), &VideoStreamNodelet::do_publish, this);      
  } catch (std::exception& e) {
    NODELET_ERROR_STREAM("Failed to start capture thread: " << e.what());
  }

    // if (activateRecord)
    // {
    //   do_saving();    
    // }


}

virtual void unsubscribe() {
  ROS_DEBUG("Unsubscribe");
  publish_timer.stop();
  capture_thread_running = false;
  capture_thread.join();


  cap.reset();
}

virtual void connectionCallbackImpl() {
  std::lock_guard<std::mutex> lock(s_mutex);
  subscriber_num++;
  if (subscriber_num == 1) {
    subscribe();
  }
}

virtual void disconnectionCallbackImpl() {
  std::lock_guard<std::mutex> lock(s_mutex);
  bool always_subscribe = false;
  pnh->getParamCached("always_subscribe", always_subscribe);
  if (video_stream_provider == "videofile" || always_subscribe) {
    return;
  }

  subscriber_num--;
  if (subscriber_num == 0) {
    unsubscribe();
  }
}

virtual void connectionCallback(const image_transport::SingleSubscriberPublisher&) {
  connectionCallbackImpl();
}

virtual void infoConnectionCallback(const ros::SingleSubscriberPublisher&) {
  connectionCallbackImpl();
}

virtual void disconnectionCallback(const image_transport::SingleSubscriberPublisher&) {
  disconnectionCallbackImpl();
}

virtual void infoDisconnectionCallback(const ros::SingleSubscriberPublisher&) {
  disconnectionCallbackImpl();
}

virtual void configCallback(video_stream_opencv_mjpg::VideoStreamConfig& new_config, uint32_t level) {
  NODELET_DEBUG("configCallback");

  if (fps > new_config.set_camera_fps) {
    NODELET_WARN_STREAM(
        "Asked to publish at 'fps' (" << fps
        << ") which is higher than the 'set_camera_fps' (" << new_config.set_camera_fps <<
        "), we can't publish faster than the camera provides images.");
    fps = new_config.set_camera_fps;
  }
  else if (new_config.set_camera_fps > origin_fps)
  {
    NODELET_WARN_STREAM("Back to good number of fps");
    fps = origin_fps;
  }

  {
    std::lock_guard<std::mutex> c_lock(c_mutex);
    std::lock_guard<std::mutex> p_lock(p_mutex);
    std::lock_guard<std::mutex> s_lock(sv_mutex);    
    config = new_config;
  }

  // show current configuration
  NODELET_INFO_STREAM("Camera name: " << new_config.camera_name);
  NODELET_INFO_STREAM("Provided camera_info_url: '" << new_config.camera_info_url << "'");
  NODELET_INFO_STREAM("Publishing with frame_id: " << new_config.frame_id);
  NODELET_INFO_STREAM("Setting camera FPS to: " << new_config.set_camera_fps);
  //NODELET_INFO_STREAM("Throttling to fps: " << new_config.fps);
  NODELET_INFO_STREAM("Setting buffer size for capturing frames to: " << new_config.buffer_queue_size);
  NODELET_INFO_STREAM("Flip horizontal image is: " << ((new_config.flip_horizontal)?"true":"false"));
  NODELET_INFO_STREAM("Flip vertical image is: " << ((new_config.flip_vertical)?"true":"false"));
  NODELET_INFO_STREAM("Video start frame is: " << new_config.start_frame);
  NODELET_INFO_STREAM("Video stop frame is: " << new_config.stop_frame);

  // if (width != 0 && new_config.height != 0)
  // {
  //   NODELET_INFO_STREAM("Forced image width is: " << new_config.width);
  //   NODELET_INFO_STREAM("Forced image height is: " << new_config.height);
  // }
  

  NODELET_INFO_STREAM("Trying to use MJPG camera stream.");


  NODELET_DEBUG_STREAM("subscriber_num: " << subscriber_num << " and level: " << level);
  if (subscriber_num > 0 && (level & 0x1))
  {
    NODELET_DEBUG("New dynamic_reconfigure config received on a parameter with configure level 1, unsubscribing and subscribing");
    unsubscribe();
    subscribe();
  }
}

virtual void onInit() {
    nh.reset(new ros::NodeHandle(getNodeHandle()));
    pnh.reset(new ros::NodeHandle(getPrivateNodeHandle()));
    subscriber_num = 0;


    // provider can be an url (e.g.: rtsp://10.0.0.1:554) or a number of device, (e.g.: 0 would be /dev/video0)
    pnh->param<std::string>("video_stream_provider", video_stream_provider, "0");

    pnh->param<int>("width", width, 1920);
    pnh->param<int>("height", height, 1080);
    pnh->param<int>("fps", fps, 60);      
    
    pnh->param<std::string>("codec", codec,"RAw");
    
    pnh->param<std::string>("backend", set_backend,"v4l2");

    // if (admissibleCodecs.find(codec) == admissibleCodecs.end() )
    // {
    //   NODELET_WARN("Codec %s not supported. Will set RAW instead.");
    // }      
    

    pnh->param<std::string>("previewName", previewName, "logitech");
    pnh->param<std::string>("path2Save", path2Save, ""); 
    pnh->param<std::string>("scriptPath", scriptPath, ""); 

    activateAudioRecord = scriptPath != "";
    activateRecord = path2Save != "";

    origin_fps = fps;

    // check file type
    try {
      int device_num = std::stoi(video_stream_provider);
      video_stream_provider_type ="videodevice";
    // get all parameters

      std::string command_parameters = "v4l2-ctl --device /dev/video" + std::to_string(device_num) + " -l";
    
      char buffer[128];
      std::string result = "";    
      FILE* pipe = popen(command_parameters.c_str(), "r");

      // read till end of process:
      while (!feof(pipe)) {

          // use buffer to read and add to result
          if (fgets(buffer, 128, pipe) != NULL)
            result += buffer;
      }

      pclose(pipe);

      std::vector<std::string> all_lines = splitString(result,"\n");

      for (int i =0; i< all_lines.size(); i++)
      {
        std::string param = splitString(all_lines[i], " ")[0];
        v4l2_params.push_back(param);

      }
    } catch (std::invalid_argument &ex) {
      if (video_stream_provider.find("http://") != std::string::npos ||
          video_stream_provider.find("https://") != std::string::npos){
        video_stream_provider_type = "http_stream";
      }
      else if(video_stream_provider.find("rtsp://") != std::string::npos){
        video_stream_provider_type = "rtsp_stream";
      }
      else{
        fs::file_type file_type = fs::status(fs::canonical(fs::path(video_stream_provider))).type();
        if(fs::path(video_stream_provider) != fs::canonical(fs::path(video_stream_provider)))
          NODELET_WARN_STREAM("Video stream provider path converted from: '" << fs::path(video_stream_provider) <<
        "' to its canonical path: '" << fs::canonical(fs::path(video_stream_provider)) << "'" );
        switch (file_type) {
        case fs::file_type::character_file:
        case fs::file_type::block_file:
          video_stream_provider_type = "videodevice";
          break;
        case fs::file_type::regular_file:
          video_stream_provider_type = "videofile";
          break;
        default:
          video_stream_provider_type = "unknown";
        }
      }
    }

    startupNotThread = true;

    // set parameters from dynamic reconfigure server
    dyn_srv = boost::make_shared<dynamic_reconfigure::Server<video_stream_opencv_mjpg::VideoStreamConfig> >(*pnh);
    auto f = boost::bind(&VideoStreamNodelet::configCallback, this, _1, _2);
    dyn_srv->setCallback(f);

    subscriber_num = 0;
    image_transport::SubscriberStatusCallback connect_cb =
      boost::bind(&VideoStreamNodelet::connectionCallback, this, _1);
    ros::SubscriberStatusCallback info_connect_cb =
      boost::bind(&VideoStreamNodelet::infoConnectionCallback, this, _1);
    image_transport::SubscriberStatusCallback disconnect_cb =
      boost::bind(&VideoStreamNodelet::disconnectionCallback, this, _1);
    ros::SubscriberStatusCallback info_disconnect_cb =
      boost::bind(&VideoStreamNodelet::infoDisconnectionCallback, this, _1);
    pub = image_transport::ImageTransport(*nh).advertiseCamera(
      "image_raw", 1,
      connect_cb, disconnect_cb,
      info_connect_cb, info_disconnect_cb,
      ros::VoidPtr(), false);

    // if (activateRecord)
    // {
    // saving_thread = boost::thread(
    //   boost::bind(&VideoStreamNodelet::do_saving, this));    
    // }
    if (activateRecord)
    {
      do_saving();    
    }

}

virtual ~VideoStreamNodelet() {
  if (subscriber_num > 0)
    subscriber_num = 0;
    unsubscribe();
    if (activateRecord)
    {    
      saving_thread_running = false;
      saving_thread.join();
    }

}
};
} // namespace

#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(video_stream_opencv_mjpg::VideoStreamNodelet, nodelet::Nodelet)
