#include <opencv2/highgui/highgui.hpp>
#include <ros/ros.h>
using namespace cv;

using namespace std;

int main(int argc, char **argv)
{
ros::init(argc, argv, "dummy_code");
ros::NodeHandle n;
Mat image;

//namedWindow("Display window");

int camID = 2;
int width = 640;
int height = 480;
int fps = 120;

string str_command = "v4l2src device=/dev/video" +to_string(camID)+" ! image/jpeg, width="+to_string(width)+", height="+to_string(height)+", framerate="+to_string(fps)+"/1, format=MJPG ! jpegdec ! videoconvert ! video/x-raw, format=BGR ! appsink";
//string str_command = "v4l2src device=/dev/video3 ! image/jpeg, width=4096, height=2160, framerate=30/1, format=MJPG ! jpegdec ! videoconvert ! video/x-raw, format=BGR ! appsink";

VideoCapture cap(str_command, cv::CAP_GSTREAMER);

//string str_command = "v4l2src device=/dev/video3 ! image/jpeg, width=4096, height=2160, framerate=30/1, format=MJPG ! jpegdec ! videoconvert ! video/x-raw, format=BGR ! appsink";
std::string saveName = "/home/elandais/test.avi";
std::string gst_out = "appsrc ! video/x-raw, format=BGR ! queue ! videoconvert ! x264enc ! h264parse ! mpegtsmux ! filesink location="+saveName;

cv::Size size(width,height);
cv::VideoWriter outfile(gst_out,cv::CAP_GSTREAMER, cv::VideoWriter::fourcc('M', 'J', 'P', 'G'), fps, size );    
// VideoCapture cap(2, cv::CAP_V4L2);
//   cap.set(cv::CAP_PROP_FOURCC, cv::VideoWriter::fourcc('M', 'J', 'P', 'G'));
//     cap.set(cv::CAP_PROP_FRAME_WIDTH, 4096);
//     cap.set(cv::CAP_PROP_FRAME_HEIGHT, 2160);
//      cap.set(cv::CAP_PROP_FPS, 30);

std::cout << cap.get(cv::CAP_PROP_FRAME_HEIGHT) << std::endl;
if (!cap.isOpened()) {

cout << "cannot open camera";

}

while (ros::ok() && cap.isOpened()) {
double begin = ros::Time::now().toSec();

cap >> image;
std::cout << "Duration : " << ros::Time::now().toSec() - begin << std::endl;
outfile << image;

// int i = 0;
// while (i < 1e7 && ros::ok())
// {
//   i+=1;
// }
// if (cap.read(image))
// {
// std::cout << "Duration : " << ros::Time::now().toSec() - begin << std::endl;

// imshow("Display window", image);
// }

// if (cap.read(image))
// {
// std::cout << "Duration : " << ros::Time::now().toSec() - begin << std::endl;

// //imshow("Display window", image);
// }



}

return 0;

}